﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class PreproductionPlaning
{
    public int Id { get; set; }

    public int? TopicId { get; set; }

    public int? CreatedBy { get; set; }

    public DateTime? CreatedAt { get; set; }

    public string? Scenario { get; set; }

    public decimal? Budget { get; set; }

    public int? ApprovedMember { get; set; }

    /// <summary>
    /// 0: Kế  hoạch chờ duyệt
    /// 1: Kế hoạch được duyệt
    /// 2: Đang thực hiện
    /// 3: Đóng sản xuất và chờ duyệt
    /// 4: Duyệt sản xuất tiền kỳ
    /// -1: Kế hoạch bị Reject
    /// -4: Sản xuất tiền kỳ bị Reject
    /// </summary>
    public int? Status { get; set; }

    public DateTime? CloseDate { get; set; }

    public decimal? CloseExpense { get; set; }

    public string? CloseNote { get; set; }

    public string? CloseReason { get; set; }

    public int? TeamId { get; set; }

    public virtual User? ApprovedMemberNavigation { get; set; }

    public virtual ICollection<PostproductionPlaning> PostproductionPlanings { get; set; } = new List<PostproductionPlaning>();

    public virtual ICollection<PreproductionExpense> PreproductionExpenses { get; set; } = new List<PreproductionExpense>();

    public virtual ICollection<PreproductionMember> PreproductionMembers { get; set; } = new List<PreproductionMember>();

    public virtual ICollection<PreproductionProgress> PreproductionProgresses { get; set; } = new List<PreproductionProgress>();

    public virtual ICollection<PreproductionSegment> PreproductionSegments { get; set; } = new List<PreproductionSegment>();

    public virtual Team? Team { get; set; }

    public virtual Topic? Topic { get; set; }
}
