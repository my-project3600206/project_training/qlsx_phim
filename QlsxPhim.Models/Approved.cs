﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class Approved
{
    public int Id { get; set; }

    public int? ObjectId { get; set; }

    public DateTime? ProcessedAt { get; set; }

    public int? ProcessedBy { get; set; }

    public string? Comment { get; set; }

    /// <summary>
    /// 1-Approved
    /// 2- Reject
    /// </summary>
    public int? Result { get; set; }

    /// <summary>
    /// 0- Đề tài
    /// 1- Đề cương
    /// 2-Kế hoạch sản xuất tiền kỳ
    /// 3- Phân đoạn kế hoạch sản xuất tiền kỳ
    /// </summary>
    public int? ObjectType { get; set; }
}
