﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class Movieapproval
{
    public int Id { get; set; }

    public int? PostProductionPlaningId { get; set; }

    public DateTime? ApprovalDate { get; set; }

    public string? Content { get; set; }

    public string? Comment { get; set; }

    public string? Suggested { get; set; }

    /// <summary>
    /// 0: Reject
    /// 1: approved
    /// </summary>
    public int? Status { get; set; }

    /// <summary>
    /// lần duyệt
    /// </summary>
    public int? No { get; set; }

    public virtual ICollection<MovieapprovalDetail> MovieapprovalDetails { get; set; } = new List<MovieapprovalDetail>();

    public virtual PostproductionPlaning? PostProductionPlaning { get; set; }
}
