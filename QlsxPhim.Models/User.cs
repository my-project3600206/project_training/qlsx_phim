﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class User
{
    public int Id { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Email { get; set; }

    public string? UserName { get; set; }

    public string? PassWord { get; set; }

    public string? Tel { get; set; }

    public virtual ICollection<MovieapprovalDetail> MovieapprovalDetails { get; set; } = new List<MovieapprovalDetail>();

    public virtual ICollection<PostproductionprogressMember> PostproductionprogressMembers { get; set; } = new List<PostproductionprogressMember>();

    public virtual ICollection<PreproductionMember> PreproductionMembers { get; set; } = new List<PreproductionMember>();

    public virtual ICollection<PreproductionPlaning> PreproductionPlanings { get; set; } = new List<PreproductionPlaning>();

    public virtual ICollection<PreproductionprogressMember> PreproductionprogressMembers { get; set; } = new List<PreproductionprogressMember>();

    public virtual ICollection<PreproductionsegmentMember> PreproductionsegmentMembers { get; set; } = new List<PreproductionsegmentMember>();

    public virtual ICollection<TeamMember> TeamMembers { get; set; } = new List<TeamMember>();

    public virtual ICollection<Team> Teams { get; set; } = new List<Team>();

    public virtual ICollection<TopicMember> TopicMembers { get; set; } = new List<TopicMember>();
}
