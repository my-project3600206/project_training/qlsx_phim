﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace QlsxPhim.Models;

public partial class QlsxPhimContext : DbContext
{
    public QlsxPhimContext()
    {
    }

    public QlsxPhimContext(DbContextOptions<QlsxPhimContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Approved> Approveds { get; set; }

    public virtual DbSet<Audit> Audits { get; set; }

    public virtual DbSet<Commoncategory> Commoncategories { get; set; }

    public virtual DbSet<Mappingfield> Mappingfields { get; set; }

    public virtual DbSet<Mappingtable> Mappingtables { get; set; }

    public virtual DbSet<Movieapproval> Movieapprovals { get; set; }

    public virtual DbSet<MovieapprovalDetail> MovieapprovalDetails { get; set; }

    public virtual DbSet<PostproductionPlaning> PostproductionPlanings { get; set; }

    public virtual DbSet<PostproductionProgress> PostproductionProgresses { get; set; }

    public virtual DbSet<PostproductionprogressMember> PostproductionprogressMembers { get; set; }

    public virtual DbSet<PreproductionExpense> PreproductionExpenses { get; set; }

    public virtual DbSet<PreproductionMember> PreproductionMembers { get; set; }

    public virtual DbSet<PreproductionPlaning> PreproductionPlanings { get; set; }

    public virtual DbSet<PreproductionProgress> PreproductionProgresses { get; set; }

    public virtual DbSet<PreproductionSegment> PreproductionSegments { get; set; }

    public virtual DbSet<PreproductionprogressMember> PreproductionprogressMembers { get; set; }

    public virtual DbSet<PreproductionsegmentMember> PreproductionsegmentMembers { get; set; }

    public virtual DbSet<Team> Teams { get; set; }

    public virtual DbSet<TeamMember> TeamMembers { get; set; }

    public virtual DbSet<Topic> Topics { get; set; }

    public virtual DbSet<TopicDocument> TopicDocuments { get; set; }

    public virtual DbSet<TopicMember> TopicMembers { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<UserRight> UserRights { get; set; }

    public virtual DbSet<VCommoncategory> VCommoncategories { get; set; }

    public virtual DbSet<Video> Videos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("server=192.168.118.185;port=3308;user=mysql;password=@Dmin4123;database=qlsx_phim");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Approved>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("approved");

            entity.Property(e => e.Comment).HasMaxLength(4000);
            entity.Property(e => e.ObjectType).HasComment("0- Đề tài\n1- Đề cương\n2-Kế hoạch sản xuất tiền kỳ\n3- Phân đoạn kế hoạch sản xuất tiền kỳ");
            entity.Property(e => e.ProcessedAt).HasColumnType("datetime");
            entity.Property(e => e.Result).HasComment("1-Approved\n2- Reject");
        });

        modelBuilder.Entity<Audit>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("audit");

            entity.Property(e => e.Action).HasMaxLength(100);
            entity.Property(e => e.AuditData)
                .HasMaxLength(2000)
                .HasComment("Json type : {\n{\n\"FieldName\":\"NumberOfTicket\",\n\"OldValue\":\"12\",\n\"NewValue\":\"8\"\n},\n{\n\"FieldName\":\"Title\",\n\"OldValue\":\"Robbier William\",\n\"NewValue\":\"Show of Robbier William\"\n},\n}");
            entity.Property(e => e.EntityName).HasMaxLength(45);
            entity.Property(e => e.ProcessedAt).HasColumnType("datetime");
            entity.Property(e => e.Type).HasComment("0- update by Viagogo webhook\n1- update by Queen of ticket website\n2- update by User");
            entity.Property(e => e.UserName).HasMaxLength(200);
        });

        modelBuilder.Entity<Commoncategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("commoncategory");

            entity.Property(e => e.Description).HasMaxLength(2000);
            entity.Property(e => e.Name).HasMaxLength(200);
            entity.Property(e => e.Type).HasComment("0-Department\\n1-Province\\n2-Districts\\n3-commune\\n4-Dai truyen hinh\\n5- Kenh truyen hinh\\n6-Loai phim\\n7-Loai de tai\\n8-Loai de cuong\\n9-Loai chi phi\\n 10-Quyen");
        });

        modelBuilder.Entity<Mappingfield>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("mappingfield");

            entity.Property(e => e.NewField).HasMaxLength(100);
            entity.Property(e => e.OriginField).HasMaxLength(100);
            entity.Property(e => e.Table).HasMaxLength(100);
        });

        modelBuilder.Entity<Mappingtable>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("mappingtable");

            entity.Property(e => e.NewTable).HasMaxLength(100);
            entity.Property(e => e.OriginTable).HasMaxLength(100);
        });

        modelBuilder.Entity<Movieapproval>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("movieapproval");

            entity.HasIndex(e => e.PostProductionPlaningId, "FK_movieapproval_postproduction_planing_idx");

            entity.Property(e => e.ApprovalDate).HasColumnType("datetime");
            entity.Property(e => e.Comment).HasMaxLength(4000);
            entity.Property(e => e.Content).HasMaxLength(4000);
            entity.Property(e => e.No).HasComment("lần duyệt");
            entity.Property(e => e.Status).HasComment("0: Reject\n1: approved");
            entity.Property(e => e.Suggested).HasMaxLength(4000);

            entity.HasOne(d => d.PostProductionPlaning).WithMany(p => p.Movieapprovals)
                .HasForeignKey(d => d.PostProductionPlaningId)
                .HasConstraintName("FK_movieapproval_postproduction_planing");
        });

        modelBuilder.Entity<MovieapprovalDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("movieapproval_detail");

            entity.HasIndex(e => e.UserId, "FK_moviceapproval_detail_User_idx");

            entity.HasIndex(e => e.MovieApprovalId, "FK_movieapproval_detail_movieapproval_idx");

            entity.Property(e => e.Comment).HasMaxLength(4000);
            entity.Property(e => e.Suggested).HasMaxLength(4000);

            entity.HasOne(d => d.MovieApproval).WithMany(p => p.MovieapprovalDetails)
                .HasForeignKey(d => d.MovieApprovalId)
                .HasConstraintName("FK_movieapproval_detail_movieapproval");

            entity.HasOne(d => d.User).WithMany(p => p.MovieapprovalDetails)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_moviceapproval_detail_User");
        });

        modelBuilder.Entity<PostproductionPlaning>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("postproduction_planing");

            entity.HasIndex(e => e.PreProductionId, "FK_postproduction_planing_preproduction_planing_idx");

            entity.Property(e => e.Budget).HasPrecision(14);
            entity.Property(e => e.CloseDate).HasColumnType("datetime");
            entity.Property(e => e.CloseNote).HasMaxLength(4000);
            entity.Property(e => e.CloseReason).HasMaxLength(2000);
            entity.Property(e => e.FromDate).HasColumnType("datetime");
            entity.Property(e => e.ToDate).HasColumnType("datetime");
            entity.Property(e => e.WorkContent).HasMaxLength(4000);

            entity.HasOne(d => d.PreProduction).WithMany(p => p.PostproductionPlanings)
                .HasForeignKey(d => d.PreProductionId)
                .HasConstraintName("FK_postproduction_planing_preproduction_planing");
        });

        modelBuilder.Entity<PostproductionProgress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("postproduction_progress");

            entity.HasIndex(e => e.PostProductionId, "FK_postproduction_progress_postproduction_plaing_idx");

            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.Expense).HasPrecision(14);
            entity.Property(e => e.FromDate).HasColumnType("datetime");
            entity.Property(e => e.Note).HasMaxLength(4000);
            entity.Property(e => e.ToDate).HasColumnType("datetime");
            entity.Property(e => e.TotalProgress).HasPrecision(14);

            entity.HasOne(d => d.PostProduction).WithMany(p => p.PostproductionProgresses)
                .HasForeignKey(d => d.PostProductionId)
                .HasConstraintName("FK_postproduction_progress_postproduction_plaing");
        });

        modelBuilder.Entity<PostproductionprogressMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("postproductionprogress_member");

            entity.HasIndex(e => e.UserId, "FK_postproductionprogress_member_user_idx");

            entity.HasIndex(e => e.PostProductionProgressId, "postproductionprogress_member_postproduction_progress_idx");

            entity.Property(e => e.Comment).HasMaxLength(4000);
            entity.Property(e => e.PercentCompleted).HasPrecision(14);
            entity.Property(e => e.Role).HasMaxLength(200);

            entity.HasOne(d => d.PostProductionProgress).WithMany(p => p.PostproductionprogressMembers)
                .HasForeignKey(d => d.PostProductionProgressId)
                .HasConstraintName("FK_postproductionprogress_member_postproduction_progress");

            entity.HasOne(d => d.User).WithMany(p => p.PostproductionprogressMembers)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_postproductionprogress_member_user");
        });

        modelBuilder.Entity<PreproductionExpense>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproduction_expense");

            entity.HasIndex(e => e.SegmentId, "FK_preproduction_expanse_preproduction_segment_idx");

            entity.HasIndex(e => e.PreProductionId, "FK_preproduction_expense_preproduction_planing_idx");

            entity.Property(e => e.Amount).HasPrecision(14);
            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.Note).HasMaxLength(4000);
            entity.Property(e => e.Reason).HasMaxLength(2000);

            entity.HasOne(d => d.PreProduction).WithMany(p => p.PreproductionExpenses)
                .HasForeignKey(d => d.PreProductionId)
                .HasConstraintName("FK_preproduction_expense_preproduction_planing");

            entity.HasOne(d => d.Segment).WithMany(p => p.PreproductionExpenses)
                .HasForeignKey(d => d.SegmentId)
                .HasConstraintName("FK_preproduction_expanse_preproduction_segment");
        });

        modelBuilder.Entity<PreproductionMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproduction_member");

            entity.HasIndex(e => e.PreProductionId, "FK_preproduction_member_preproduction_planing_idx");

            entity.HasIndex(e => e.MemberId, "FK_preproduction_member_user_idx");

            entity.Property(e => e.Description).HasMaxLength(4000);
            entity.Property(e => e.Role).HasMaxLength(200);

            entity.HasOne(d => d.Member).WithMany(p => p.PreproductionMembers)
                .HasForeignKey(d => d.MemberId)
                .HasConstraintName("FK_preproduction_member_user");

            entity.HasOne(d => d.PreProduction).WithMany(p => p.PreproductionMembers)
                .HasForeignKey(d => d.PreProductionId)
                .HasConstraintName("FK_preproduction_member_preproduction_planing");
        });

        modelBuilder.Entity<PreproductionPlaning>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproduction_planing");

            entity.HasIndex(e => e.TeamId, "FK_preproduction_planing_team_idx");

            entity.HasIndex(e => e.TopicId, "FK_preproduction_planing_topic_idx");

            entity.HasIndex(e => e.ApprovedMember, "FK_preproduction_planing_user_idx");

            entity.Property(e => e.Budget).HasPrecision(14);
            entity.Property(e => e.CloseDate).HasColumnType("datetime");
            entity.Property(e => e.CloseExpense).HasPrecision(14);
            entity.Property(e => e.CloseNote).HasMaxLength(4000);
            entity.Property(e => e.CloseReason).HasMaxLength(2000);
            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.Status).HasComment("0: Kế  hoạch chờ duyệt\n1: Kế hoạch được duyệt\n2: Đang thực hiện\n3: Đóng sản xuất và chờ duyệt\n4: Duyệt sản xuất tiền kỳ\n-1: Kế hoạch bị Reject\n-4: Sản xuất tiền kỳ bị Reject");

            entity.HasOne(d => d.ApprovedMemberNavigation).WithMany(p => p.PreproductionPlanings)
                .HasForeignKey(d => d.ApprovedMember)
                .HasConstraintName("FK_preproduction_planing_user");

            entity.HasOne(d => d.Team).WithMany(p => p.PreproductionPlanings)
                .HasForeignKey(d => d.TeamId)
                .HasConstraintName("FK_preproduction_planing_team");

            entity.HasOne(d => d.Topic).WithMany(p => p.PreproductionPlanings)
                .HasForeignKey(d => d.TopicId)
                .HasConstraintName("FK_preproduction_planing_topic");
        });

        modelBuilder.Entity<PreproductionProgress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproduction_progress");

            entity.HasIndex(e => e.PreProductionId, "FK_preproduction_progress_preproduction_planing_idx");

            entity.HasIndex(e => e.SegmentId, "FK_preproduction_progress_preproduction_segment_idx");

            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.Expense).HasPrecision(14);
            entity.Property(e => e.FromDate).HasColumnType("datetime");
            entity.Property(e => e.Note).HasMaxLength(4000);
            entity.Property(e => e.SegmentProgress).HasPrecision(14);
            entity.Property(e => e.ToDate).HasColumnType("datetime");
            entity.Property(e => e.TotalProgress).HasPrecision(14);

            entity.HasOne(d => d.PreProduction).WithMany(p => p.PreproductionProgresses)
                .HasForeignKey(d => d.PreProductionId)
                .HasConstraintName("FK_preproduction_progress_preproduction_planing");

            entity.HasOne(d => d.Segment).WithMany(p => p.PreproductionProgresses)
                .HasForeignKey(d => d.SegmentId)
                .HasConstraintName("FK_preproduction_progress_preproduction_segment");
        });

        modelBuilder.Entity<PreproductionSegment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproduction_segment");

            entity.HasIndex(e => e.CommuneId, "FK_preproduction_segment_commune_idx");

            entity.HasIndex(e => e.DistrictId, "FK_preproduction_segment_districti_idx");

            entity.HasIndex(e => e.PreProductionId, "FK_preproduction_segment_preproduction_planing_idx");

            entity.HasIndex(e => e.ProvinceId, "FK_preproduction_segment_province_idx");

            entity.Property(e => e.Address).HasMaxLength(200);
            entity.Property(e => e.Budget).HasPrecision(14);
            entity.Property(e => e.FromDate).HasColumnType("datetime");
            entity.Property(e => e.Status).HasComment("0- Not processed\n1-In Processed\n2- Rejected\n3- Approved");
            entity.Property(e => e.ToDate).HasColumnType("datetime");

            entity.HasOne(d => d.Commune).WithMany(p => p.PreproductionSegmentCommunes)
                .HasForeignKey(d => d.CommuneId)
                .HasConstraintName("FK_preproduction_segment_commune");

            entity.HasOne(d => d.District).WithMany(p => p.PreproductionSegmentDistricts)
                .HasForeignKey(d => d.DistrictId)
                .HasConstraintName("FK_preproduction_segment_districti");

            entity.HasOne(d => d.PreProduction).WithMany(p => p.PreproductionSegments)
                .HasForeignKey(d => d.PreProductionId)
                .HasConstraintName("FK_preproduction_segment_preproduction_planing");

            entity.HasOne(d => d.Province).WithMany(p => p.PreproductionSegmentProvinces)
                .HasForeignKey(d => d.ProvinceId)
                .HasConstraintName("FK_preproduction_segment_province");
        });

        modelBuilder.Entity<PreproductionprogressMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproductionprogress_member");

            entity.HasIndex(e => e.PreProductionProgressId, "FK_preproductionprogress_member_preoproduction_progress_idx");

            entity.HasIndex(e => e.UserId, "FK_preproductionprogress_member_user_idx");

            entity.Property(e => e.Comment).HasMaxLength(4000);
            entity.Property(e => e.PercentComplete).HasPrecision(14);
            entity.Property(e => e.Role).HasMaxLength(200);
            entity.Property(e => e.Status).HasComment("0- Không hoàn thành\n1- Hoàn thành\n2- Hoàn thành 1 phần");

            entity.HasOne(d => d.PreProductionProgress).WithMany(p => p.PreproductionprogressMembers)
                .HasForeignKey(d => d.PreProductionProgressId)
                .HasConstraintName("FK_preproductionprogress_member_preoproduction_progress");

            entity.HasOne(d => d.User).WithMany(p => p.PreproductionprogressMembers)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_preproductionprogress_member_user");
        });

        modelBuilder.Entity<PreproductionsegmentMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("preproductionsegment_member");

            entity.HasIndex(e => e.PreProductionSegmentId, "FK_preproductionsegment_member_preproduction_segment_idx");

            entity.HasIndex(e => e.UserId, "FK_preproductionsegment_member_user_idx");

            entity.Property(e => e.Description).HasMaxLength(4000);
            entity.Property(e => e.Role).HasMaxLength(200);

            entity.HasOne(d => d.PreProductionSegment).WithMany(p => p.PreproductionsegmentMembers)
                .HasForeignKey(d => d.PreProductionSegmentId)
                .HasConstraintName("FK_preproductionsegment_member_preproduction_segment");

            entity.HasOne(d => d.User).WithMany(p => p.PreproductionsegmentMembers)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_preproductionsegment_member_user");
        });

        modelBuilder.Entity<Team>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("team");

            entity.HasIndex(e => e.LeaderId, "FK_team_user_idx");

            entity.Property(e => e.Description).HasMaxLength(4000);
            entity.Property(e => e.Name).HasMaxLength(200);

            entity.HasOne(d => d.Leader).WithMany(p => p.Teams)
                .HasForeignKey(d => d.LeaderId)
                .HasConstraintName("FK_team_user");
        });

        modelBuilder.Entity<TeamMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("team_member");

            entity.HasIndex(e => e.TeamId, "FK_team_member_team_idx");

            entity.HasIndex(e => e.UserId, "FK_team_member_user_idx");

            entity.Property(e => e.Role).HasMaxLength(200);

            entity.HasOne(d => d.Team).WithMany(p => p.TeamMembers)
                .HasForeignKey(d => d.TeamId)
                .HasConstraintName("FK_team_member_team");

            entity.HasOne(d => d.User).WithMany(p => p.TeamMembers)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_team_member_user");
        });

        modelBuilder.Entity<Topic>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("topic");

            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(2000);
            entity.Property(e => e.EstimatedBegin).HasColumnType("datetime");
            entity.Property(e => e.EstimatedBroadcasting).HasColumnType("datetime");
            entity.Property(e => e.EstimatedBudget).HasPrecision(14);
            entity.Property(e => e.EstimatedEnd).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(200);
            entity.Property(e => e.ParentId).HasComment("-Đề tài : ParentId=0\n-Đề cương: ParentId là Id của đề tài xuất phát");
            entity.Property(e => e.Scenario).HasMaxLength(4000);
            entity.Property(e => e.Status).HasComment("0- Not process\n1-In Proccess\n2-Reject\n3- Aprrove");
            entity.Property(e => e.Type).HasComment("0-Đề tài\n1-Đề cương");
        });

        modelBuilder.Entity<TopicDocument>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("topic_document");

            entity.HasIndex(e => e.TopicId, "FK_topic_document_topic_idx");

            entity.Property(e => e.Description).HasMaxLength(2000);
            entity.Property(e => e.FileUrl).HasMaxLength(200);
            entity.Property(e => e.Key).HasMaxLength(200);
            entity.Property(e => e.Value).HasMaxLength(2000);

            entity.HasOne(d => d.Topic).WithMany(p => p.TopicDocuments)
                .HasForeignKey(d => d.TopicId)
                .HasConstraintName("FK_topic_document_topic");
        });

        modelBuilder.Entity<TopicMember>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("topic_member");

            entity.HasIndex(e => e.TopicId, "FK_topic_member_topic_idx");

            entity.HasIndex(e => e.MemberId, "FK_topic_member_user_idx");

            entity.Property(e => e.Description).HasMaxLength(2000);
            entity.Property(e => e.Role).HasMaxLength(200);

            entity.HasOne(d => d.Member).WithMany(p => p.TopicMembers)
                .HasForeignKey(d => d.MemberId)
                .HasConstraintName("FK_topic_member_user");

            entity.HasOne(d => d.Topic).WithMany(p => p.TopicMembers)
                .HasForeignKey(d => d.TopicId)
                .HasConstraintName("FK_topic_member_topic");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("user");

            entity.Property(e => e.Email).HasMaxLength(200);
            entity.Property(e => e.FirstName).HasMaxLength(200);
            entity.Property(e => e.LastName).HasMaxLength(200);
            entity.Property(e => e.PassWord).HasMaxLength(200);
            entity.Property(e => e.Tel).HasMaxLength(100);
            entity.Property(e => e.UserName).HasMaxLength(100);
        });

        modelBuilder.Entity<UserRight>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("user_right");
        });

        modelBuilder.Entity<VCommoncategory>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("v_commoncategory");

            entity.Property(e => e.Description).HasMaxLength(2000);
            entity.Property(e => e.Grand).HasMaxLength(200);
            entity.Property(e => e.GrandId).HasDefaultValueSql("'0'");
            entity.Property(e => e.Name).HasMaxLength(200);
            entity.Property(e => e.ParentName).HasMaxLength(200);
            entity.Property(e => e.Type).HasComment("0-Department\\n1-Province\\n2-Districts\\n3-commune\\n4-Dai truyen hinh\\n5- Kenh truyen hinh\\n6-Loai phim\\n7-Loai de tai\\n8-Loai de cuong\\n9-Loai chi phi\\n 10-Quyen");
        });

        modelBuilder.Entity<Video>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("video");

            entity.Property(e => e.Note).HasMaxLength(4000);
            entity.Property(e => e.VideoLength).HasPrecision(14);
            entity.Property(e => e.VideoName).HasMaxLength(200);
            entity.Property(e => e.VideoSize).HasPrecision(14);
            entity.Property(e => e.VideoUrl).HasMaxLength(200);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
