﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class Topic
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public string? Scenario { get; set; }

    public DateTime? EstimatedBegin { get; set; }

    public DateTime? EstimatedEnd { get; set; }

    public DateTime? EstimatedBroadcasting { get; set; }

    public decimal? EstimatedBudget { get; set; }

    public DateTime? CreatedAt { get; set; }

    public int? CreatedBy { get; set; }

    /// <summary>
    /// 0- Not process
    /// 1-In Proccess
    /// 2-Reject
    /// 3- Aprrove
    /// </summary>
    public int? Status { get; set; }

    /// <summary>
    /// 0-Đề tài
    /// 1-Đề cương
    /// </summary>
    public int? Type { get; set; }

    /// <summary>
    /// -Đề tài : ParentId=0
    /// -Đề cương: ParentId là Id của đề tài xuất phát
    /// </summary>
    public int? ParentId { get; set; }

    public virtual ICollection<PreproductionPlaning> PreproductionPlanings { get; set; } = new List<PreproductionPlaning>();

    public virtual ICollection<TopicDocument> TopicDocuments { get; set; } = new List<TopicDocument>();

    public virtual ICollection<TopicMember> TopicMembers { get; set; } = new List<TopicMember>();
}
