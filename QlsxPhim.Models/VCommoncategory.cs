﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class VCommoncategory
{
    public int Id { get; set; }

    public string? Name { get; set; }

    /// <summary>
    /// 0-Department\n1-Province\n2-Districts\n3-commune\n4-Dai truyen hinh\n5- Kenh truyen hinh\n6-Loai phim\n7-Loai de tai\n8-Loai de cuong\n9-Loai chi phi\n 10-Quyen
    /// </summary>
    public int? Type { get; set; }

    public string? Description { get; set; }

    public int? ParentId { get; set; }

    public string? ParentName { get; set; }

    public int? GrandId { get; set; }

    public string? Grand { get; set; }
}
