﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class PreproductionProgress
{
    public int Id { get; set; }

    public int? PreProductionId { get; set; }

    public int? SegmentId { get; set; }

    public int? CreatedBy { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? FromDate { get; set; }

    public string? Note { get; set; }

    public decimal? SegmentProgress { get; set; }

    public decimal? TotalProgress { get; set; }

    public DateTime? ToDate { get; set; }

    public decimal? Expense { get; set; }

    public virtual PreproductionPlaning? PreProduction { get; set; }

    public virtual ICollection<PreproductionprogressMember> PreproductionprogressMembers { get; set; } = new List<PreproductionprogressMember>();

    public virtual PreproductionSegment? Segment { get; set; }
}
