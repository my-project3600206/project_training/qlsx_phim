﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class Team
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public int? LeaderId { get; set; }

    public virtual User? Leader { get; set; }

    public virtual ICollection<PreproductionPlaning> PreproductionPlanings { get; set; } = new List<PreproductionPlaning>();

    public virtual ICollection<TeamMember> TeamMembers { get; set; } = new List<TeamMember>();
}
