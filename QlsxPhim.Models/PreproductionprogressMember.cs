﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class PreproductionprogressMember
{
    public int Id { get; set; }

    public int? PreProductionProgressId { get; set; }

    public int? UserId { get; set; }

    public string? Role { get; set; }

    public string? Comment { get; set; }

    /// <summary>
    /// 0- Không hoàn thành
    /// 1- Hoàn thành
    /// 2- Hoàn thành 1 phần
    /// </summary>
    public int? Status { get; set; }

    public decimal? PercentComplete { get; set; }

    public virtual PreproductionProgress? PreProductionProgress { get; set; }

    public virtual User? User { get; set; }
}
