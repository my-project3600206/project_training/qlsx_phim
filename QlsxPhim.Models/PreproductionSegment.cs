﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class PreproductionSegment
{
    public int Id { get; set; }

    public int? PreProductionId { get; set; }

    public string? Scenario { get; set; }

    public int? ProvinceId { get; set; }

    public int? DistrictId { get; set; }

    public int? CommuneId { get; set; }

    public string? Address { get; set; }

    public DateTime? FromDate { get; set; }

    public DateTime? ToDate { get; set; }

    public decimal? Budget { get; set; }

    /// <summary>
    /// 0- Not processed
    /// 1-In Processed
    /// 2- Rejected
    /// 3- Approved
    /// </summary>
    public int? Status { get; set; }

    public virtual Commoncategory? Commune { get; set; }

    public virtual Commoncategory? District { get; set; }

    public virtual PreproductionPlaning? PreProduction { get; set; }

    public virtual ICollection<PreproductionExpense> PreproductionExpenses { get; set; } = new List<PreproductionExpense>();

    public virtual ICollection<PreproductionProgress> PreproductionProgresses { get; set; } = new List<PreproductionProgress>();

    public virtual ICollection<PreproductionsegmentMember> PreproductionsegmentMembers { get; set; } = new List<PreproductionsegmentMember>();

    public virtual Commoncategory? Province { get; set; }
}
