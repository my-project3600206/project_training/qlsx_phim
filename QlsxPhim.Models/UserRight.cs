﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class UserRight
{
    public int Id { get; set; }

    public int? RightId { get; set; }

    public int? UserId { get; set; }
}
