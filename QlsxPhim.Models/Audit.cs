﻿using System;
using System.Collections.Generic;

namespace QlsxPhim.Models;

public partial class Audit
{
    public int Id { get; set; }

    public DateTime? ProcessedAt { get; set; }

    /// <summary>
    /// 0- update by Viagogo webhook
    /// 1- update by Queen of ticket website
    /// 2- update by User
    /// </summary>
    public int? Type { get; set; }

    public string? UserName { get; set; }

    public string? EntityName { get; set; }

    public int? EntityId { get; set; }

    /// <summary>
    /// Json type : {
    /// {
    /// &quot;FieldName&quot;:&quot;NumberOfTicket&quot;,
    /// &quot;OldValue&quot;:&quot;12&quot;,
    /// &quot;NewValue&quot;:&quot;8&quot;
    /// },
    /// {
    /// &quot;FieldName&quot;:&quot;Title&quot;,
    /// &quot;OldValue&quot;:&quot;Robbier William&quot;,
    /// &quot;NewValue&quot;:&quot;Show of Robbier William&quot;
    /// },
    /// }
    /// </summary>
    public string? AuditData { get; set; }

    public string? Action { get; set; }
}
