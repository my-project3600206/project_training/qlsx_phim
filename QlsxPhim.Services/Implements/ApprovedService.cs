﻿using Microsoft.EntityFrameworkCore;
using QlsxPhim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QlsxPhim.Services.Implements
{
    public class ApprovedService : BaseService<Approved>, IApprovedService
    {
        public ApprovedService(QlsxPhimContext context) : base(context) { }


        public Task<int> CreateAsync(Approved entity)
        {
            if(entity == null)
            {
                throw new NotImplementedException(nameof(entity));
            }
            _context.Approveds.Add(entity);
            return _context.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(Approved entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public ValueTask DisposeAsync()
        {
            return _context.DisposeAsync();
        }

        public IQueryable<Approved> GetAll()
        {
            return _context.Approveds;
        }

        public ValueTask<Approved> GetObjAsync(int id)
        {
            return _context.Approveds.FindAsync(id);
        }

        public Task<IQueryable<Approved>> GetSingleAsync(int id)
        {
            return Task.FromResult(_context.Approveds.Where(x => x.Id == id));
        }

        public Task<int> UpdateAsync(Approved entity)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<Approved>> GetlistExportExcel()
        {
            return _context.Approveds;
        }
    }
   
}
