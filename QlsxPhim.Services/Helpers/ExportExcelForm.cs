﻿using QlsxPhim.Services.Contansts;
using Syncfusion.Drawing;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QlsxPhim.Services.Helpers
{
    public class ExportExcelForm
    {
        public static CustomFile ExportExcel(List<Dictionary<string, object>> myObjects)
        {
            var FileName = "Output.xlsx";
            var ContentType = "application /vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var pathFile = @"Output.xlsx";

            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            application.DefaultVersion = ExcelVersion.Excel2016;
            IWorkbook workbook = application.Workbooks.Create(1);
            IWorksheet sheet = workbook.Worksheets.Create("data");

            #region Define Style

            IStyle formatheader = workbook.Styles.Add("HeaderStyle");
            IStyle formatTitle = workbook.Styles.Add("TitleStyle");
            IStyle formatTable = workbook.Styles.Add("TableStyle");
            formatheader.Font.FontName = "Calibri";
            formatheader.Font.RGBColor = Color.Red;
            formatheader.Font.Size = 18;
            formatheader.Font.Bold = true;
            formatheader.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            formatheader.VerticalAlignment = ExcelVAlign.VAlignCenter;

            formatTitle.Font.Color = ExcelKnownColors.White;
            formatTitle.Font.Bold = true;
            formatTitle.Font.Size = 12;
            formatTitle.Font.FontName = "Calibri";
            formatTitle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            formatTitle.VerticalAlignment = ExcelVAlign.VAlignCenter;
            formatTitle.Color = Color.FromArgb(0, 118, 147, 60);
            formatTitle.Borders[ExcelBordersIndex.EdgeLeft].LineStyle = ExcelLineStyle.Thin;
            formatTitle.Borders[ExcelBordersIndex.EdgeRight].LineStyle = ExcelLineStyle.Thin;
            formatTitle.Borders[ExcelBordersIndex.EdgeTop].LineStyle = ExcelLineStyle.Thin;
            formatTitle.Borders[ExcelBordersIndex.EdgeBottom].LineStyle = ExcelLineStyle.Thin;

            formatTable.Font.Color = ExcelKnownColors.Black;
            formatTable.Font.Bold = true;
            formatTable.Font.Size = 12;
            formatTable.Font.FontName = "Calibri";
            formatTable.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            formatTable.VerticalAlignment = ExcelVAlign.VAlignCenter;
            formatTable.Color = Color.LightGray;
            formatTable.Borders[ExcelBordersIndex.EdgeLeft].LineStyle = ExcelLineStyle.Thin;
            formatTable.Borders[ExcelBordersIndex.EdgeRight].LineStyle = ExcelLineStyle.Thin;
            formatTable.Borders[ExcelBordersIndex.EdgeTop].LineStyle = ExcelLineStyle.Thin;
            formatTable.Borders[ExcelBordersIndex.EdgeBottom].LineStyle = ExcelLineStyle.Thin;

            #endregion

            Stream excelStream = File.Create(Path.GetFullPath(pathFile));

            // validate list 
            if (myObjects == null || myObjects.Count == 0)
            {
                workbook.SaveAs(excelStream);
                excelStream.Dispose();
                var fileBytes = File.ReadAllBytes(pathFile);
                return new CustomFile()
                {
                    FileContents = fileBytes,
                    FileName = FileName,
                    ContentType = ContentType,
                };
            }

            for (int i = 0; i < myObjects.Count; i++)
            {
                var item = myObjects[i];
                int index = 1;
                if (item != null)
                {
                    foreach (var e in item)
                    {
                        if (i == 0)
                        {
                            // add title cho bang
                            sheet.Range[i + 5, index].Text = e.Key;
                        }
                        // add value
                        sheet.Range[i + 6, index].Value2 = e.Value;
                        //format table
                        sheet.Range[i + 6, index].CellStyle = formatTable;
                        index++;
                    }

                    sheet.UsedRange.AutofitColumns();
                    // format table
                    for (int j = 0; j < item.Count; j++)
                    {
                        sheet.Range[5, j + 1].CellStyle = formatTitle;
                    }


                }
            }
            //Save the file in the given path
            workbook.SaveAs(excelStream);
            excelStream.Dispose();
            var contentFile = File.ReadAllBytes(pathFile);
            // delete file in project
            if (File.Exists(pathFile))
            {
                File.Delete(pathFile);
            }
            return new CustomFile()
            {
                FileContents = contentFile,
                FileName = FileName,
                ContentType = ContentType,
            };
        }

    }
}
