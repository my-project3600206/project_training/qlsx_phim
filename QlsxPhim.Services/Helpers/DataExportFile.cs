﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QlsxPhim.Services.Helpers
{
    public class DataExportFile
    {
        public static List<Dictionary<string, object>> GetDataExport<T>(List<T> MyObject)
        {
            var ListData = new List<Dictionary<string, object>>();
            foreach (object obj in MyObject)
            {
                var item = GetItem(obj);
                ListData.Add(item);
            }

            return ListData;
        }

        public static Dictionary<string, object> GetItem(object obj)
        {
            if (obj == null)
                return new Dictionary<string, object>();

            PropertyInfo[] props = obj.GetType().GetProperties();
            Dictionary<string, object> objectResult = new Dictionary<string, object>();
            foreach (PropertyInfo item in props)
            {
                object value = item.GetValue(obj, new object[] { });
                objectResult.Add(item.Name, value);
            }
            return objectResult;
        }

    }
}
