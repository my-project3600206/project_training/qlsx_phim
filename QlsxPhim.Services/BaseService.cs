﻿using QlsxPhim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QlsxPhim.Services
{
    public class BaseService<T> : IService<T> where T : class
    {
        public readonly QlsxPhimContext _context;

        public BaseService(QlsxPhimContext context) 
        {
            _context = context;
        }

        public Task<int> CreateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public ValueTask DisposeAsync()
        {
            throw new NotImplementedException();
        }

     

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }


        public Task<IEnumerable<T>> GetlistExportExcel(T entity)
        {
            throw new NotImplementedException();
        }

        public ValueTask<T> GetObjAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<T>> GetSingleAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(T entity, T newEntity, string email, int type)
        {
            throw new NotImplementedException();
        }
    }
}
