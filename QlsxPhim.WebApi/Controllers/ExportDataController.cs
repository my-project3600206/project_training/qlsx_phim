﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using MySqlX.XDevAPI.Common;
using MySqlX.XDevAPI.Relational;
using NuGet.Packaging;
using QlsxPhim.Models;
using QlsxPhim.Services;
using QlsxPhim.Services.Helpers;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace QlsxPhim.WebApi.Controllers
{
    
    public class ExportDataController : QOTOdataController
    {
        private readonly QlsxPhimContext _context;

        private readonly IApprovedService _service;

        public ExportDataController(IApprovedService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("GetFile")]
        public async Task<object> Get()
        {
            
            try
            {
                var approvedObj = new List<Approved>() {
                    new Approved
                {
                    Id = 1,
                    ObjectId = 1,
                    ProcessedAt = DateTime.Now,
                    ProcessedBy = 1,
                    Comment = "khong co comment",
                    Result = 1,
                    ObjectType = 1,

                }, new Approved
                {
                    Id = 3,
                    ObjectId = 3,
                    ProcessedAt = DateTime.Now,
                    ProcessedBy = 31,
                    Comment = "khong co comment",
                    Result = 32,
                    ObjectType = 1,

                }
                };
                
                var dataExport = DataExportFile.GetDataExport<Approved>(approvedObj);
                var exportExcel = ExportExcelForm.ExportExcel(dataExport);
                return File(exportExcel.FileContents, exportExcel.ContentType, exportExcel.FileName);
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, "internal server error: "+ ex.Message);
            }

        }

        
    }
}
