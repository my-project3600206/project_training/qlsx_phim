﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using QlsxPhim.Models;
using QlsxPhim.Services;


namespace QlsxPhim.WebApi.Controllers
{
    public class ApprovedsController : QOTOdataController
    {
        private readonly IApprovedService _service;
       

        public ApprovedsController(IApprovedService service )
        {
            _service = service;
           
        }

        [HttpGet]
        [EnableQuery()]
        public async Task<IActionResult> Get()
        {
            var query = await Task.FromResult(_service.GetAll());
            return Ok(query);
        }

        [HttpGet]
        [EnableQuery()]
        public async Task<IActionResult> Get([FromODataUri] int key)
        {
            var query = await _service.GetSingleAsync(key);
            return Ok(SingleResult.Create(query));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Approved entity)
        {
            await _service.CreateAsync(entity);
            return Created(entity);
        }

        [HttpPost]
        public async Task<IActionResult> Put([FromODataUri] int key, [FromBody] Delta<Approved> entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var original = await _service.GetObjAsync(key);
            if (original == null)
            {
                return NotFound();
            }
            entity.Put(original);
            await _service.UpdateAsync(original);
            return Updated(original);
        }

        [HttpPatch]
        public async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<Approved> entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var original = await _service.GetObjAsync(key);
            if (original == null)
            {
                return NotFound();
            }
            entity.Patch(original);
            await _service.UpdateAsync(original);
            return Updated(original);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromODataUri] int key)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);    // về cơ bản sẽ cho bạn biết liệu có bất kỳ vấn đề nào với dữ liệu của bạn được đăng lên máy chủ hay không, dựa trên các chú thích dữ liệu được thêm vào thuộc tính mô hình của bạn.

            var original = await _service.GetObjAsync(key);
            if (original == null)
                return NotFound();

            await _service.DeleteAsync(original);
            return Ok();

        }

       
    }
}
